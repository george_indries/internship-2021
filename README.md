# Evaluare Services

* REST API Evaluation Application for Candidates

* Web application for HR department
     * Candidates’ selection for a certain position within the company

* Features
    * Register and enroll in active evaluation sessions
    * View problem requirements
    * Load solution to the proposed problem
