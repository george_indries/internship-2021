package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.axonsoft.evl.facade.attendance_year.AttendanceYearFacade;

import java.util.List;

@RestController
@RequestMapping(AttendanceYearController.BASE_URL)
public class AttendanceYearController {
    protected static final String BASE_URL = "evaluation/attendance-years";

    private final AttendanceYearFacade attendanceYearFacade;

    public AttendanceYearController(AttendanceYearFacade attendanceYearFacade) {
        this.attendanceYearFacade = attendanceYearFacade;
    }

    @GetMapping("/list")
    public ResponseEntity<List<String>> getQualificationLevels() {
        return new ResponseEntity<>(attendanceYearFacade.findAllAttendanceYears(), HttpStatus.OK);
    }
}
