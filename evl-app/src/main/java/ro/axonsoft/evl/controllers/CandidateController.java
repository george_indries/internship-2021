package ro.axonsoft.evl.controllers;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ro.axonsoft.evl.dtos.candidate.CandidateDto;
import ro.axonsoft.evl.enums.ExceptionType;
import ro.axonsoft.evl.exceptions.CandidateAlreadyRegisteredException;
import ro.axonsoft.evl.exceptions.InternalServerException;
import ro.axonsoft.evl.exceptions.InvalidSessionException;
import ro.axonsoft.evl.facade.candidate.CandidateFacade;

import java.util.*;

@RestController
@RequestMapping(CandidateController.BASE_URL)
public class CandidateController {

    protected static final String BASE_URL = "evaluation/candidates";
    @Autowired
    private Environment environment;

    private final CandidateFacade candidateFacade;

    public CandidateController(CandidateFacade candidateFacade) {
        this.candidateFacade = candidateFacade;
    }

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public CandidateDto createCandidate(@RequestBody CandidateDto candidateDto) {

        String urlServer = environment.getProperty("keycloak.auth-server-url");
        String realm = candidateDto.getEvlSessionId();
        Keycloak keycloak = Keycloak.getInstance(urlServer, "master", "admin", "admin", "admin-cli");

        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(candidateDto.getEmail());
        user.setEmail(candidateDto.getEmail());
        user.setEmailVerified(true);
        user.setFirstName(candidateDto.getFirstName());
        user.setLastName(candidateDto.getLastName());
        user.setRealmRoles(Collections.singletonList("USER"));

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(candidateDto.getPassword());
        user.setCredentials(Collections.singletonList(credentialRepresentation));

        UserRepresentation userFound = keycloak.realm(realm).users().search(candidateDto.getEmail()).stream().findFirst().orElse(null);
        if(userFound != null) {
            throw new CandidateAlreadyRegisteredException(ExceptionType.CANDIDATE_ALREADY_REGISTERED.name());
        }
        keycloak.realm(realm).users().create(user);

        try {
            return candidateFacade.save(candidateDto);
        } catch (InternalServerException ex) {
            deleteUserFromAuthServer(keycloak, realm, getUserByUsernameFromAuthServer(keycloak, realm, candidateDto.getEmail()).getId());
            throw new InternalServerException(ex.getMessage());
        } catch (InvalidSessionException ex) {
            deleteUserFromAuthServer(keycloak, realm, getUserByUsernameFromAuthServer(keycloak, realm, candidateDto.getEmail()).getId());
            throw new InvalidSessionException(ex.getMessage());
        } catch (CandidateAlreadyRegisteredException ex) {
            deleteUserFromAuthServer(keycloak, realm, getUserByUsernameFromAuthServer(keycloak, realm, candidateDto.getEmail()).getId());
            throw new CandidateAlreadyRegisteredException(ex.getMessage());
        }

    }

    private void deleteUserFromAuthServer(Keycloak keycloak, String realm, String id) {
        keycloak.realm(realm).users().delete(id);
    }

    private UserRepresentation getUserByUsernameFromAuthServer(Keycloak keycloak, String realm, String email) {
        return keycloak.realm(realm).users().search(email).stream().findFirst().orElse(null);
    }

}
