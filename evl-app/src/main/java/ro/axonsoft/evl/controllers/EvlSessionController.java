package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.axonsoft.evl.dtos.evlsession.EvlSessionDto;
import ro.axonsoft.evl.enums.PeriodType;
import ro.axonsoft.evl.facade.evlsession.EvlSessionFacade;

import java.util.List;

@RestController
@RequestMapping(EvlSessionController.BASE_URL)
public class EvlSessionController {

    protected static final String BASE_URL = "evaluation/sessions";

    private final EvlSessionFacade evlSessionFacade;

    public EvlSessionController(EvlSessionFacade evlSessionFacade) {
        this.evlSessionFacade = evlSessionFacade;
    }

    @GetMapping("/list")
    public ResponseEntity<List<EvlSessionDto>> getAllSessions(@RequestParam(name = "period_type") String periodType) {
        return new ResponseEntity<>(evlSessionFacade.filterByPeriodType(PeriodType.valueOf(periodType)), HttpStatus.OK);
    }
}
