package ro.axonsoft.evl.controllers;

import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(LoginController.BASE_URL)
@CrossOrigin
public class LoginController {

    protected static final String BASE_URL = "evaluation/login/";

    @PostMapping("token")
    public ResponseEntity<Object> getToken() {
        SimpleKeycloakAccount token = (SimpleKeycloakAccount) SecurityContextHolder.getContext().getAuthentication().getDetails();
        return ResponseEntity.ok(token.getKeycloakSecurityContext().getTokenString());
    }
}
