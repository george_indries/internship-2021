package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.axonsoft.evl.dtos.major.MajorDto;
import ro.axonsoft.evl.facade.major.MajorFacade;

import java.util.List;

@RestController
@RequestMapping(MajorController.BASE_URL)
public class MajorController {

    protected static final String BASE_URL = "evaluation/majors";

    private final MajorFacade majorFacade;

    public MajorController(MajorFacade majorFacade) {
        this.majorFacade = majorFacade;
    }

    @GetMapping
    public ResponseEntity<List<MajorDto>> getMajorsByUniversityIdAndQualificationLevel(@RequestParam(name = "universityId") String universityId,
                                                                                       @RequestParam(name = "qualificationLevel", required = false) String qualificationLevel) {
        return new ResponseEntity<>(majorFacade.findByUniversityIdAndQualificationLevel(universityId, qualificationLevel), HttpStatus.OK);
    }
}
