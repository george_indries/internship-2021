package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.axonsoft.evl.dtos.problem.ProblemDto;
import ro.axonsoft.evl.enums.ExceptionType;
import ro.axonsoft.evl.exceptions.InvalidFileNameException;
import ro.axonsoft.evl.exceptions.InvalidSolutionFileException;
import ro.axonsoft.evl.facade.problem.ProblemFacade;

import java.io.IOException;

@RestController
@RequestMapping(ProblemController.BASE_URL)
public class ProblemController {

    protected static final String BASE_URL = "evaluation/problems";

    private final ProblemFacade problemFacade;

    public ProblemController(ProblemFacade problemFacade) {
        this.problemFacade = problemFacade;
    }

    @GetMapping
    public ResponseEntity<String> getProblemForCandidate(@RequestParam(name = "evlSessionId") String evlSessionId) {
        ProblemDto problemDto = problemFacade.findByEvaluationSessionId(evlSessionId);
        String problem = problemFacade.problemMdToHTMLConversion(problemDto);
        return new ResponseEntity<>(problem, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> getSolution(@RequestBody MultipartFile file, @RequestParam(value = "email") String email, @RequestParam(value = "evlSessionId") String sessionId) throws Exception {

        if(file == null || file.getOriginalFilename() == null || file.isEmpty()) {
            throw new InvalidFileNameException(ExceptionType.INVALID_FILE_NAME.toString());
        }

        if(!file.getOriginalFilename().contains(".zip")) {
            throw new HttpMediaTypeNotAcceptableException(ExceptionType.INVALID_FILE_EXTENSION.toString());
        }

        try {
            problemFacade.saveSolution(file.getOriginalFilename(), file.getBytes(), email, sessionId);
            return new ResponseEntity<>("Solution Sent Successfully.", HttpStatus.OK);
        } catch (IOException exception) {
            throw new InvalidSolutionFileException(ExceptionType.INVALID_SOLUTION_FILE.toString());
        }

    }
}
