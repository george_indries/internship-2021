package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.axonsoft.evl.facade.qualification_level.QualificationLevelFacade;

import java.util.List;

@RestController
@RequestMapping(QualificationLevelController.BASE_URL)
public class QualificationLevelController {
    protected static final String BASE_URL = "evaluation/qualification-levels";

    private final QualificationLevelFacade qualificationLevelFacade;

    public QualificationLevelController(QualificationLevelFacade qualificationLevelFacade) {
        this.qualificationLevelFacade = qualificationLevelFacade;
    }

    @GetMapping("/list")
    public ResponseEntity<List<String>> getAllQualificationLevels() {

        return new ResponseEntity<>(qualificationLevelFacade.findAllQualificationLevels(), HttpStatus.OK);
    }
}
