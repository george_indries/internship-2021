package ro.axonsoft.evl.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.axonsoft.evl.dtos.university.UniversityDto;
import ro.axonsoft.evl.facade.university.UniversityFacade;

import java.util.List;

@RestController
@RequestMapping(UniversityController.BASE_URL)
public class UniversityController {

    protected static final String BASE_URL = "evaluation/universities";

    private final UniversityFacade universityFacade;

    public UniversityController(UniversityFacade universityFacade) {
        this.universityFacade = universityFacade;
    }

    @GetMapping("/list")
    public ResponseEntity<List<UniversityDto>> getAllUniversities() {
        return new ResponseEntity<>(universityFacade.findAll(), HttpStatus.OK);
    }
}
