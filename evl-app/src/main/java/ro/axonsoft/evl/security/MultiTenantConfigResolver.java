package ro.axonsoft.evl.security;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.representations.adapters.config.AdapterConfig;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import ro.axonsoft.evl.enums.RequestType;

import java.io.IOException;

@Component
public class MultiTenantConfigResolver extends KeycloakSpringBootConfigResolver implements KeycloakConfigResolver {

    private final AdapterConfig keycloakConfig;

    public MultiTenantConfigResolver(AdapterConfig keycloakConfig) {
        this.keycloakConfig = keycloakConfig;
    }

    @Override
    public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {

        AdapterConfig currentConfig;

        if(request.getURI().contains("evaluation/login/token")) {
            currentConfig = loadAdapterConfigByRequestType(RequestType.AUTHENTICATION);
        } else {
            currentConfig = loadAdapterConfigByRequestType(RequestType.AUTHORIZATION);
        }
        currentConfig.setRealm(request.getQueryParamValue("evlSessionId"));
        BeanUtils.copyProperties(keycloakConfig, currentConfig);

        return KeycloakDeploymentBuilder.build(currentConfig);
    }

    private AdapterConfig loadAdapterConfigByRequestType(RequestType requestType) {
        AdapterConfig currentConfig = null;
        try {
            currentConfig = KeycloakDeploymentBuilder.loadAdapterConfig(
                    new ClassPathResource(getFileName(requestType)).getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentConfig;
    }

    private String getFileName(RequestType requestType) {
        return requestType.name().toLowerCase() + ".json";
    }

}
