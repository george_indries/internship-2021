package ro.axonsoft.evl.domain;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ro.axonsoft.evl.domain.base.EntitiesConfiguration;

@Configuration
@Import(EntitiesConfiguration.class)
@ComponentScan(basePackages="ro.axonsoft.evl.domain.*")
public class DomainAutoConfiguration {
}
