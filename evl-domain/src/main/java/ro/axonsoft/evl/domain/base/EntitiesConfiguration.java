package ro.axonsoft.evl.domain.base;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages="ro.axonsoft.evl.domain.*")
@EntityScan(basePackages="ro.axonsoft.evl.domain.*")
public class EntitiesConfiguration {

    @Autowired
    private Optional<EntityUuidGenerator> entityUuidGenerator;

    @PostConstruct
    public void setupEntityUuidGeneratorHolder() {
        EntityUuidGeneratorHolder.setEntityUuidGenerator(entityUuidGenerator.orElseGet(UrlBase64EntityUuidGenerator::new));
    }
}
