package ro.axonsoft.evl.domain.base;

import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public interface EntityUuidGenerator {

    String newUuid();
}
