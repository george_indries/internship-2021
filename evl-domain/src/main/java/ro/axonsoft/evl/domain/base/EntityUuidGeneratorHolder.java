package ro.axonsoft.evl.domain.base;

public abstract class EntityUuidGeneratorHolder {

    private static EntityUuidGenerator entityUuidGenerator;

    public static void setEntityUuidGenerator(final EntityUuidGenerator uuidGenerator) {
        entityUuidGenerator = uuidGenerator;
    }

    public static EntityUuidGenerator getEntityUuidGenerator() {
        return entityUuidGenerator;
    }

    /**
     * Hidden constructor.
     */
    private EntityUuidGeneratorHolder() {
        throw new AssertionError("Non-instantiable class");
    }

}
