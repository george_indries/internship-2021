package ro.axonsoft.evl.domain.base;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SrgKeyEntityTml {

    @Id
    @Column(name = "ID", nullable = false, unique = true, updatable = false, length = 100)
    private String id = EntityUuidGeneratorHolder.getEntityUuidGenerator().newUuid();


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (entityRefClass().isInstance(obj)) {
            final SrgKeyEntityTml other = (SrgKeyEntityTml) obj;
            return Objects.equals(id, other.getId());
        } else {
            return false;
        }
    }

    protected String entityShortName() {
        return entityRefClass().getSimpleName().toLowerCase();
    }

    protected abstract Class<? extends SrgKeyEntityTml> entityRefClass();

    @Override
    public String toString() {
        return String.format("%s#%s", entityShortName(), id);
    }
}
