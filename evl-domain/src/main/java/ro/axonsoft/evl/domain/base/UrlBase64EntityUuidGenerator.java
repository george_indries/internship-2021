package ro.axonsoft.evl.domain.base;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

import javax.annotation.concurrent.ThreadSafe;

import com.google.common.io.BaseEncoding;

@ThreadSafe
public class UrlBase64EntityUuidGenerator implements EntityUuidGenerator {

    private final BaseEncoding encoding = BaseEncoding.base64Url().omitPadding();

    @Override
    public String newUuid() {

        final UUID uuid = UUID.randomUUID();
        final ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());

        final byte[] uuidBytes = bb.array();
        final byte[] guidBytes = Arrays.copyOf(uuidBytes, uuidBytes.length);

        guidBytes[0] = uuidBytes[3];
        guidBytes[1] = uuidBytes[2];
        guidBytes[2] = uuidBytes[1];
        guidBytes[3] = uuidBytes[0];
        guidBytes[4] = uuidBytes[5];
        guidBytes[5] = uuidBytes[4];
        guidBytes[6] = uuidBytes[7];
        guidBytes[7] = uuidBytes[6];

        return encoding.encode(guidBytes);
    }
}
