package ro.axonsoft.evl.domain.candidate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.*;
import ro.axonsoft.evl.domain.base.SrgKeyEntityTml;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.major.Major;
import ro.axonsoft.evl.domain.university.University;
import ro.axonsoft.evl.enums.AttendanceYear;

import java.time.LocalDateTime;

@Entity
@Table(name = "EVL_CND")
@Getter
@Setter
public class Candidate extends SrgKeyEntityTml {

    @NotNull
    @Column(name = "FNM")
    private String firstName;

    @NotNull
    @Column(name = "LNM")
    private String lastName;

    @NotNull
    @Column(name = "EML")
    private String email;

    @NotNull
    @Column(name = "PSWD")
    private String password;

    @NotNull
    @Column(name = "UNVN")
    private String universityName;

    @NotNull
    @Column(name = "MJN")
    private String majorName;

    @NotNull
    @Column(name = "QL")
    private String qualificationLevel;

    @NotNull
    @Column(name = "AY")
    @Enumerated(EnumType.STRING)
    private AttendanceYear attendanceYear;

    @NotNull
    @Column(name = "REGDT")
    private LocalDateTime registrationDate;

    @Column(name = "DELDT")
    private LocalDateTime deliveryDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNVID")
    private University university;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MJID")
    private Major major;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVLSESID")
    private EvlSession evlSession;

    @Override
    protected Class<? extends SrgKeyEntityTml> entityRefClass() {
        return Candidate.class;
    }
}
