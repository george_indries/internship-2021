package ro.axonsoft.evl.domain.candidate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;


public interface CandidateRepository extends JpaRepository<Candidate, String> {

    @Query("select case " +
            "when count(candidate) > 0 then true " +
            "else false " +
            "end " +
            "from Candidate candidate where candidate.email = :email and candidate.evlSession.id = :evlSessionId")
    boolean existsByEmailAndSessionId(@Param("email") String email, @Param("evlSessionId") String evlSessionId);

    Candidate findByEmail(String email);

    @Modifying
    @Query("update Candidate c set c.deliveryDate = :deliveryDate where c.id = :id")
    void update(@Param(value = "id") String id, @Param(value = "deliveryDate") LocalDateTime deliveryDate);
}