package ro.axonsoft.evl.domain.evlsession;

import lombok.Getter;
import lombok.Setter;
import ro.axonsoft.evl.domain.base.SrgKeyEntityTml;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.problem.Problem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EVL_EVLSES")
@Getter
@Setter
public class EvlSession extends SrgKeyEntityTml {

    @NotNull
    @Column(name = "N")
    private String name;

    @NotNull
    @Column(name = "REGST")
    private LocalDateTime registrationStart;

    @NotNull
    @Column(name = "REGEND")
    private LocalDateTime registrationEnd;

    @NotNull
    @Column(name = "DELST")
    private LocalDateTime delStart;

    @NotNull
    @Column(name = "DELEND")
    private LocalDateTime delEnd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PBID")
    private Problem problem;

    @OneToMany(mappedBy = "evlSession")
    private Set<Candidate> candidates = new HashSet<>();

    @Override
    protected Class<? extends SrgKeyEntityTml> entityRefClass() {
        return EvlSession.class;
    }
}
