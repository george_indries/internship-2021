package ro.axonsoft.evl.domain.evlsession;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;


public interface EvlSessionRepository extends JpaRepository<EvlSession, String> {

    @Query("FROM EvlSession evl WHERE evl.registrationStart <= :current AND evl.registrationEnd >= :current")
    List<EvlSession> findAllActiveSessionsForRegistration(LocalDateTime current);

    @Query("FROM EvlSession evl WHERE evl.delStart <= :current AND evl.delEnd >= :current")
    List<EvlSession> findAllActiveSessionsForDelivery(LocalDateTime current);
}
