package ro.axonsoft.evl.domain.major;

import lombok.Getter;
import lombok.Setter;
import ro.axonsoft.evl.domain.base.SrgKeyEntityTml;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.university.University;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EVL_MJ")
@Getter
@Setter
public class Major extends SrgKeyEntityTml {

    @NotNull
    @Column(name = "N")
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNVID")
    private University university;

    @NotNull
    @Column(name = "QL")
    //@Enumerated(EnumType.STRING)
    private String qualificationLevel;

    @OneToMany(mappedBy = "major")
    private Set<Candidate> candidates = new HashSet<>();

    @Override
    protected Class<? extends SrgKeyEntityTml> entityRefClass() {
        return Major.class;
    }
}
