package ro.axonsoft.evl.domain.major;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MajorRepository extends JpaRepository<Major, String> {

    @Query("FROM Major m WHERE m.university.id = :universityId")
    List<Major> findByUniversityId(@Param("universityId") String universityId);

    @Query("FROM Major m WHERE m.university.id = :universityId AND m.qualificationLevel = :qualificationLevel")
    List<Major> findByUniversityIdAndQualificationLevel(@Param("universityId") String universityId,
                                                        @Param("qualificationLevel") String qualificationLevel);
}
