package ro.axonsoft.evl.domain.problem;

import lombok.Getter;
import lombok.Setter;
import ro.axonsoft.evl.domain.base.SrgKeyEntityTml;
import ro.axonsoft.evl.domain.evlsession.EvlSession;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EVL_PB")
@Getter
@Setter
public class Problem extends SrgKeyEntityTml {

    @NotNull
    @Column(name = "TXT")
    private String text;

    @Column(name = "N")
    private String name;

    @Column(name = "DESCR")
    private String description;

    @OneToMany(mappedBy = "problem")
    private Set<EvlSession> evlSessions = new HashSet<>();

    @Override
    protected Class<? extends SrgKeyEntityTml> entityRefClass() {
        return Problem.class;
    }
}
