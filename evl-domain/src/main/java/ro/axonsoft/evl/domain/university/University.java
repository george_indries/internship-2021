package ro.axonsoft.evl.domain.university;

import lombok.Getter;
import lombok.Setter;
import ro.axonsoft.evl.domain.base.SrgKeyEntityTml;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.major.Major;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EVL_UNV")
@Getter
@Setter
public class University extends SrgKeyEntityTml {

    @NotNull
    @Column(name = "N")
    private String name;

    @OneToMany(mappedBy = "university")
    private Set<Major> majors = new HashSet<>();

    @OneToMany(mappedBy = "university")
    private Set<Candidate> candidates = new HashSet<>();

    @Override
    protected Class<? extends SrgKeyEntityTml> entityRefClass() {
        return University.class;
    }
}
