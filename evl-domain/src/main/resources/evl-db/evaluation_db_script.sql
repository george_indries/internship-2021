CREATE TABLE EVL_PB
(
	ID VARCHAR(30),
	TXT TEXT not null,
	N VARCHAR(100),
	DESCR VARCHAR(200),
	constraint EVL_PB_PK primary key (ID)
);

create table EVL_UNV
(
	ID VARCHAR(30),
	N VARCHAR(100) not null unique,
	constraint EVL_UNV_PK primary key (ID)
);

create table EVL_EVLSES
(
	ID VARCHAR(30),
	N VARCHAR(100) not null,
	REGST DATE not null,
	REGEND DATE not null,
	DELST DATE not null,
	DELEND DATE not null,
	PBID VARCHAR(30),
	constraint EVL_EVLSES_PK primary key (ID),
	constraint EVL_EVLSES_PB_FK foreign key (PBID) references EVL_PB(ID)
);

create table EVL_MJ
(
	ID VARCHAR(30),
	N VARCHAR(200) not null,
	UNVID VARCHAR(30) not null,
	QL VARCHAR(30) not null,
	constraint EVL_MJ_PK primary key (ID),
	constraint EVL_MJ_UNV_FK foreign key (UNVID) references EVL_UNV(ID),
	constraint ELV_MJ_UNVID_QL_N_UN unique (UNVID, QL, N)
);

create table EVL_CND
(
	ID VARCHAR(40),
	FNM VARCHAR(100) not null,
	LNM VARCHAR(100) not null,
	EML VARCHAR(200) not null unique,
	PSWD VARCHAR(100) not null,
	UNVN VARCHAR(100) not null,
	MJN VARCHAR(200) not null,
	QL VARCHAR(30) not null,
	AY VARCHAR(30) not null,
	REGDT DATE not null,
	DELDT DATE,
	UNVID VARCHAR(30),
	MJID VARCHAR(30),
	EVLSESID VARCHAR(30) not null,
	constraint EVL_CND_PK primary key (ID),
	constraint ELV_CND_EVLSESID_EML_UN unique (EVLSESID, EML),
	constraint EVL_CND_EVLSES_FK foreign key (EVLSESID) references EVL_EVLSES(ID),
	constraint EVL_CND_MJ_FK foreign key (MJID) references EVL_MJ(ID),
	constraint EVL_CND_UNV_FK foreign key (UNVID) references EVL_UNV(ID)
);
