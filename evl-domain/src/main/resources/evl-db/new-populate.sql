 delete from evl_cnd where id is not null;
 
 delete from evl_evlses where id is not null;
 
 delete from evl_mj where id is not null;
 
 delete from evl_pb where id is not null;
 
 delete from evl_unv where id is not null;

/*Populate problems*/
 insert into evl_pb (id, txt, n, descr)
 values('W02sEusXv0ejYl5aZPDnuQ',
		'
> “The secret of getting ahead is getting started.” – Mark Twain
## Cuprins

1. [Cerințe funcționale](#cerințe-funcționale) 
2. [Cerințe tehnice](#cerințe-tehnice)
3. [Date de intrare](#date-de-intrare)
4. [Date de ieșire](#date-de-ieșire)
5. [Cerințe suplimentare](#cerințe-suplimentare)
6. [Cerințe detaliate](#cerințe-detaliate)
7. [Despre livrare și evaluare](#despre-livrare-și-evaluare)



## Cerințe funcționale <!--id="cerințe-funcționale"-->
Departamentul de marketing al unei companii care deține un magazin online dorește să obțină o serie de date statistice despre plățile efectuate prin intermediul site-ului.

Astfel, metricele solicitate de departamentul de marketing sunt:
* media valorilor plăților;
* numărul de plăți cu valoare până în 5000RON (inclusiv);
* numărul de plăți cu valoare mai mare de 5000RON;
* numărul de plăți efectuate de persoane ce nu au împlinit vârsta majoratului;
* suma totală a plăților efectuate de cetățeni români născuți în București;
* numărul de cetățeni străini ce au efectuat plăți.

Departamentul comercial acceptă să pună la dispoziția programatorului plățile încasate, dar fără a dezvălui identitatea completă a clienților ce au efectuat aceste plăți. Astfel datele ce pot fi puse la dispoziția programtorului sunt:
* CNP-ul plătitorului și
* valoarea plății.

## Cerințe tehnice <!--id="cerințe-tehnice"-->

Să se dezvolte o aplicație Java care să proceseze datele despre plățile puse la dispoziție de departamentul comercial și să extragă informațiile solicitate de departamentul de marketing.

Având în vedere că în trecut s-a constatat faptul că uneori CNP-urile clienților ce ajung la nivelul plăților nu suntvalide și, în plus, datele puse la dispoziție de departamentul comercial vor fi procesate manual,

se cere ca pe lângă informațiile solicitate de departamentul de marketing, în datele de ieșire să apară și erorile deprocesare.

## Date de intrare <!--id="date-de-intrare"-->
* Datele de intrare se găsesc într-un fișier csv folosind ca separator de câmpuri.
* Fiecare linie din acest fișier reprezintă o plată.
* În cadrul unei linii, pe prima poziție se află CNP-ul, iar pe a doua suma plății.
* Suma plăţii folosește caracterul "." (punct) ca separator de zecimale.
* Este posibil ca în fișier să apară linii goale. Acestea trebuie ignorate.

**Exemplu:**

1930107189436;15324.98

2961022513249;1860

1960608409971;18.1

@startuml

start
  :Procesare CSV;
repeat 
    if (Fișier valid) then (Da)
        :Apelare procesare date;
        :Parsare linie;
        if (Date invalide) then (Da)
            :Afișare eroare;
        else (Nu)
            :Procesare date;
            :Adăugare la fișierul rezultat;
        endif
    else (Nu)
        :Afișare eroare;
        stop
    endif
repeat while (Fișier gol) is (Da) not (Nu)
stop

@enduml


## Date de ieșire <!--id="date-de-ieșire"-->

Aplicația va furniza datele solicitate de departamentul de marketing și erorile găsite în datele de intrare într-unformat structurat descris de următorul tip abstract de date:


```java
PayMetrics { 

    averagePaymentAmount: BigDecimal, 

    smallPayments: Integer, 
    
    bigPayments: Integer, 
    
    paymentsByMinors: Integer, 
    
    totalAmountCapitalCity: BigDecimal, 
    
    foreigners: Integer, 
    
    errors: List of { 
        
        line: Integer, 
        
        type: Integer 
        
    } 
}
```


unde:
* averagePaymentAmount reprezintă *media valorilor plăților;*
* smallPayments este
*numărul de plăți cu valoare până în 5000RON (inclusiv);*
* bigPayments este *numărul de plăți cu valoare mai mare de 5000RON;*
* paymentsByMinors este *numărul de plăți efectuate de persoane ce nu au împlinit vârsta majoratului;*
* totalAmountCapitalCity este *suma totală a plăților efectuate de cetățeni români născuți în București;*
* foreigners este *numărul de cetățeni străini ce au efectuat plăți;*
* errors este lista de erori, o eroare având proprietățile:
    * line - numărul liniei la care a apărut eroarea;
    * type - tipul erorii; una din valorile:
        * 0 - linia este invalidă, i.e. nu conține două valori separate prin;
        * 1 - CNP-ul nu este valid;
        * 2 - suma plății nu este validă.

Datele din această structură vor fi serializate într-un fișier.

## Cerințe suplimentare <!--id="cerințe-suplimentare"-->

Având în vedere că metricele ce trebuie extrase din aceste date de intrare se pot schimba la un moment dat și faptul că pot apărea alte situații în care e nevoie să se valideze CNP-uri și să se extragă informații din ele, se cereimplementarea validării și interpretării unui CNP într-un API distinct, reutilizabil cu următoarea structură:


```java
interface CnpValidator {
/**
* Valideza CNP-ul primit ca parametru si returneaza partile componente ale acestuia.
*
* @param cnp
*           CNP-ul de validat
* @return partile componente ale CNP-ului
* @throws CnpException
*           daca CNP-ul nu este valid
*/
CnpParts validateCnp(String cnp) throws CnpException;

}

interface CnpParts {
/**
* Sexul determinat pe baza primei cifrei din CNP.
*/
Sex sex();

/**
* Posesorul CNP-ului este cetatean strain?
*
* @return {@code true} daca este cetatean strain, {@code false} in caz
*           contrar
*/
Boolean foreigner();

/**
* Judetul.
*/
Judet judet();

/**
* Data nasterii.
*/
CalDate birthDate();

/**
* Numarul de ordine.
*/
Short orderNumber(); 

}

interface CalDate {
    Short year();
    
    Byte month();
    
    Byte day(); 
}
```


## Cerințe detaliate <!--id="cerințe-detaliate"-->

Toate clasele și interfețele trebuie să fie în sub-pachete ale package-ului ro.axonsoft.internship21
* clasele și interfețele referitoare la validarea CNP-ului vor fi în sub-package-ul cnp,
* iar cele referitoare la procesarea plăților în pay;

Implementarea interfeței CnpValidator se va face într-o clasă cu numele CnpValidatorImpl în același package.

În ceea ce privește procesarea plăților, aceasta se va face într-o clasă PayMetricsProcessorImpl implementând următoarea interfață:


```java
interface PayMetricsProcessor {
/**
* Proceseaza platile din {@code paymentsInputStream} si scrie metricele in
* {@code metricsOutputStream }
*
* @param paymentsInputStream
*           input stream al fisierului csv conținând plățile
* @param metricsOutputStream
*           output stream al fișierului in care se serializeaza
*           obiectul conținând metricile și erorile
* @throws IOException
*           daca apare o eroare de I/O.
*/
void process (InputStream paymentsInputStream OutputStream metricsOutputStream) throws IOException

}
```


Obiectul serializat în `OutputStream` trebuie să implementeze următoarea interfață:


```java
interface PayMetrics {

/**
* Numarul de cetateni straini care au efectuat plati.
*/
Integer foreigners();

/**
* Numarul de plati efectuate de catre minori.
*/
Integer paymentsByMinors();

/**
* Numarul de plati peste pragul de 5000RON.
*/
Integer bigPayments();

/**
* Numarul de plati pana in 5000RON inclusiv.
*/
Integer smallPayments();

/**
* Media sumelor tuturor platilor. Valoarea are maxim doua zecimale.
*/
BigDecimal averagePaymentAmount();

/**
* Totalul platilor efectuate de cetatenii romani nascuti in Bucuresti.
*/
BigDecimal totalAmountCapitalCity();

/**
* Erorile de procesare.
*/
Set<PayError> errors(); 
}

public interface PayError
{

/**
* Numarul liniei la care s-a obtinut eroarea.
*/
Integer line();

/**
* Tipul erorii:
* <ul>
* <li>0 linie invalida</li>
* <li>1 pentru CNP invalid</li>
* <li>2 pentru suma plata invalida</li>
* </ul>
*/
Integer type(); 
}
```


**Descrierea formatului CNP este disponibilă [aici](https://ro.wikipedia.org/wiki/Cod_numeric_personal_(Rom%C3%A2nia)).**

În implementare se pot folosii oricare din bibliotecile open-source, disponibile pe mavenCentral.

**Diagrama API-ului validatorului de CNP-uri**

@startuml
interface           CalDate {
    +Short year()
    +Byte month()
    +Byte day()
}
interface           CnpParts {
    +Sex sex()
    +Boolean foreigner()
    +CalDate birthDate()
    +Judet judet()
    +Integer orderNumber()
}
interface           CnpValidator {
    +CnpParts validateCnp(in cnp: String)
}
enum                Sex {
    M
    F
    U
}
enum                Judet {
    AB
    CJ
    BU
}

class               CnpException

CnpException <-[plain] CnpValidator : throws
CnpParts <|-- CnpValidator
CalDate <-[dotted] CnpParts
Sex <-[plain] CnpParts
Judet <-[plain] CnpParts

note bottom of Sex
    {U pentru sex necunoscut - cazul cetățenilor
    străini cu prima cifră din CNP 9}
end note

note bottom of Judet
    {Va conține element două litere standard
    pentru toate județele tării}
end note
@enduml

**Diagrama API-ului procesatorului de plăţi**

@startuml
interface           PayMetricsProcessor {
    +process(in payIn: InputStream, in metrics: OutputStream)
}
class               PayMetricsProcessorImpl
class               IOException
interface           CnpValidator
interface           PayMetrics {
    +BigDecimal averagePaymentAmount()
    +Integer bigPayments()
    +Integer smallPayments()
    +Integer paymentsByMinors()
    +BigDecimal totalAmountCapitalCity()
    +Integer foreigners()
    +PayError[] errors()
}
interface           PayError {
    +Integer line()
    +Integer type()
}
PayMetricsProcessor <-[dotted] PayMetricsProcessorImpl
PayError <|- PayMetrics
PayError <|-- PayMetricsProcessorImpl
IOException <-[plain] PayMetricsProcessor : throws
CnpValidator <-[dotted] PayMetricsProcessorImpl

note top of PayMetricsProcessor
  Serializeaza in OutputStream
 un obiect implementand PayMetrics
end note

@enduml

## Despre livrare și evaluare <!--id="despre-livrare-și-evaluare"-->

## Livrare
* Livrarea se va face sub forma unei arhive zip cu numele [nume]-[prenume].zip
    * conținând un director src cu package-urile de surse
    * dacă folosiți biblioteci externe de pe mavenCentral treceți numele lor într-un fișier cu numele dependencies inclus în arhivă.
    * dacă aveți precizări cu privire la codul vostru sau ați avut nelămuriri cu privire la cerințe, treceți-le într-un fișier readme inclus în arhivă.
* Încărcaţi arhiva prin intermediul acestei aplicaţii accesând secţiunea
Trimite.


## Evaluare
* Se vor evalua respectarea cerințelor cu privire la:
    * validatorul de CNP
    * procesarea tranzacțiilor.

* În mavenCentral există cel puțin o bibliotecă implementând validarea de CNP-uri. Dacă nu implementați propriul vostru validator de CNP-uri puteți să folosiți una din acestea sau să copiați codul open-source, caz în care va trebui să menționați acestea în dependencies sau readme.
* Puncte bonus vor fi acordate pentru:
    * cod ușor de citit, respectând paradigmele OOP şi bunele practici de programare
    * folosirea cu rost a cât mai multor API-uri din JDK
    * folosirea cu rost a unor biblioteci utilitare externe (exceptând implementarea validatorului de CNP)
    * folosirea sintaxei Java 8
    * comentarea codului, în special în format javadoc
    * scrierea de teste unitare

| Important         |         Detalii                   |
| ----------------- | --------------------------------- |
| Structura         |       Respectarea regulilor       |
| Formatare cod     |       Cod documentat              |
| Bonus             |       Respectare paradigme OOP    |

[Vezi documentația Java](https://docs.oracle.com/en/java/ "Java Doc")

**Mult succes!**',
 'Evaluare Internship 2021', 
 'Aplicație Java care procesează datele despre plățile puse la dispoziție de departamentul comercial și extrage informațiile solicitate de departamentul de marketing al unei companii.'),
 ('2HZobZTU0kO80jUGt_k4lA',
 '## Cuprins

1. [Cerințe funcționale](#cerințe-funcționale) 
2. [Cerințe tehnice](#cerințe-tehnice)
3. [Date de intrare](#date-de-intrare)
4. [Cerințe detaliate](#cerințe-detaliate)
5. [Despre livrare și evaluare](#despre-livrare-și-evaluare)


## Cerințe funcționale <!--id="cerințe-funcționale"-->
YBank (Banking Management System) este o aplicație utilizată în scopuri bancare normale și este capabilă să îndeplinească următoarele funcții:

* Creare cont
* Vizualizare profil
* Editare profil
* Depunere bani
* Transfer bani dintr-un cont în altul
* Retragere bani dintr-un cont
* Vizualizare lista de clienți
* Vizualizare tranzacții
* Schimbare pin-ul

## Cerințe tehnice <!--id="cerințe-tehnice"-->

Să se dezvolte o aplicație de management bancar în Java care să proceseze datele despre tranzacțiile bancare ale utilizatorilor. Cerințe suplimentare:

* O bancă are multe sucursale; o sucursală este desemnată ca *sediu central zonal care supraveghează celelalte sucursale din acea zonă*.
* Fiecare sucursală poate avea mai multe conturi și împrumuturi.
* Un cont poate fi fie un *cont de economii*, fie un *cont curent*.
* Un client poate deschide atât un cont de economii, cât și un cont curent. Cu toate acestea, un client nu trebuie să aibă mai multe conturi de economii sau cont curent. De asemenea, un client poate achiziționa împrumuturi de la bancă.

Aplicația trebuie să dispună de GUI (Graphical User Interface) și de conexiune la o bază de date MySQL.

## Date de intrare <!--id="date-de-intrare"-->
* Datele de intrare se găsesc într-un fișier CSV folosind ca separator de câmpuri.
* Fiecare linie din acest fișier reprezintă o plată.
* În cadrul unei linii, pe prima poziție se află CNP-ul, iar pe a doua suma plății.
* Suma plăţii folosește caracterul "." (punct) ca separator de zecimale.
* Este posibil ca în fișier să apară linii goale. Acestea trebuie ignorate.

## Cerințe detaliate <!--id="cerințe-detaliate"-->

@startuml
class               Bank {
    +boolean addAccount(Account account)
    +boolean removeAccount(Account account)
    +boolean addClient(Client client)
    +boolean removeClient(Client client)
    +double credit()
    +double debit()
}

class               Client {
    -id: String
    -name: String
    -address: String
    +String getName()
    +String getAddress()
    +String getID()
}

class               Account {
    -id: String
    -number: Long
    -balance: Double
    +depositMoney(Double value)
    +withdrawal(Double value)
    +Long getNumber()
    +Double getBalance()
}

class               Saving {
    +interest()
}

class               ClientCollection {
    +boolean add(Client client)
    +boolean remove(Client client)
}

class               AccountCollection {
    +boolean add(Account account)
    +boolean remove(Account account)
}

ClientCollection "1" <-[plain] Bank : +clients
AccountCollection <-[plain] Bank : +accounts
Account "0..*" <-[plain] AccountCollection : +account
Account <-- Saving
Client "0..*" <-[plain] ClientCollection : +client
Client <-[plain] Account : +owner
@enduml

***Example for the Account class***

```java
public class Account {
    private final String id;
    private Long number;
    private Double balance;

    public void depositMoney(double depositAmount){
        this.balance += depositAmount;
    }

    public void withdrawal(double withdrawalAmount){
        if(this.balance < withdrawalAmount) {
            System.out.println("You do not have enough funds.");
        } else {
            this.balance -= withdrawalAmount;
        }
    }

    public Long getNumber(){
        return this.number;
    }

    public Double getBalance(){
        return this.balance;
    }
}

```

## Despre livrare și evaluare <!--id="despre-livrare-și-evaluare"-->

## Livrare
* Livrarea se va face sub forma unei arhive zip cu numele [nume]-[prenume].zip
    * conținând un director src cu package-urile de surse
    * dacă folosiți biblioteci externe de pe mavenCentral treceți numele lor într-un fișier cu numele dependencies inclus în arhivă.
    * dacă aveți precizări cu privire la codul vostru sau ați avut nelămuriri cu privire la cerințe, treceți-le într-un fișier readme inclus în arhivă.
* Încărcaţi arhiva prin intermediul acestei aplicaţii accesând secţiunea
Trimite.


## Evaluare
* Se vor evalua respectarea cerințelor cu privire la:
    * realizarea tuturor acțiunilor posibile
    * folosirea cu rost a librariilor externe

* Puncte bonus vor fi acordate pentru:
    * cod ușor de citit, respectând paradigmele OOP şi bunele practici de programare
    * folosirea cu rost a cât mai multor API-uri din JDK
    * folosirea cu rost a unor biblioteci utilitare externe
    * folosirea sintaxei Java 8
    * comentarea codului, în special în format javadoc
    * scrierea de teste unitare

| Important         |         Detalii                   |
| ----------------- | --------------------------------- |
| Structura         |       Respectarea regulilor       |
| Formatare cod     |       Cod documentat              |
| Bonus             |       Respectare paradigme OOP    |

[Vezi documentația Java](https://docs.oracle.com/en/java/ "Java Doc")


**Mult succes!**',
 'Evaluare Internship 2020',
 'Dezvoltarea unei aplicatii desktop de management bancar ce contine toate operatiile de baza pe care un utilizator le poate efectua asupra contului sau bancar.');
 


/*Populate universities*/
 insert into evl_unv (id, n)
 values
 ('tcKjwZ5sc0W38PNcZVFf9Q', 'UBB'),
 ('eKCAcyihP0qCgiRlGnPMzw', 'UTCN'),
 ('cE-807DPkEaWYrjBUgW4VA', 'UPB'),
 ('GyNwnyiyWkK3xkrIgI7drQ', 'UAIC'),
 ('o0PzzRsIIEiVkQnRzr2svw', 'UVT');
 
 /*Populate majors*/
 insert into evl_mj (id, n, unvid, ql)
 values
 ('sXm6Tdh2K0CV12gNz0nv2g', 'Automatica', 'eKCAcyihP0qCgiRlGnPMzw', 'BACHELOR'),
 ('1NrN_dtKD0SKHW27rS81cQ', 'Calculatoare', 'eKCAcyihP0qCgiRlGnPMzw', 'BACHELOR'),
 ('Lh9qKtBWBkSx-BY2pgRIEw', 'Informatica', 'tcKjwZ5sc0W38PNcZVFf9Q', 'BACHELOR'),
 ('U4NYm7nowkeuE0SeO66eLA', 'Matematica', 'tcKjwZ5sc0W38PNcZVFf9Q', 'BACHELOR'),
 ('LV_bdOmLJ0iUq3T7Tqp-jg', 'Automatica', 'cE-807DPkEaWYrjBUgW4VA', 'BACHELOR'),
 ('KqvMM5jlzkKfn_nP_zWVyA', 'Calculatoare', 'cE-807DPkEaWYrjBUgW4VA', 'BACHELOR'),
 ('3xad6xhMpESd_pRTHYcyXg', 'Telecomunicatii', 'cE-807DPkEaWYrjBUgW4VA', 'BACHELOR'),
 ('qGuA-nASWki49OgaIGeN3g', 'Tehnologia Informatiei', 'cE-807DPkEaWYrjBUgW4VA', 'BACHELOR'),
 ('mixisX7mhUWkBySGH-a9Uw', 'Informatica', 'o0PzzRsIIEiVkQnRzr2svw', 'BACHELOR'),
 ('kYCGXrNsIES-PrfVFh04fA', 'Matematica', 'o0PzzRsIIEiVkQnRzr2svw', 'BACHELOR'),
 ('-C6uOiHY0EWDPtq-JIyIoA', 'Informatica', 'GyNwnyiyWkK3xkrIgI7drQ', 'BACHELOR'),
 ('eFHfde9XCUOnfP4fP_fs-w', 'Matematica', 'GyNwnyiyWkK3xkrIgI7drQ', 'BACHELOR'),
 ('dDfK6JTRtEmC6c1WbL-Ndw', 'Inginerie Software', 'eKCAcyihP0qCgiRlGnPMzw', 'MASTER'),
 ('y-XytrA0l0eRK9_-1nOwbw', 'Informatica Aplicata', 'eKCAcyihP0qCgiRlGnPMzw', 'MASTER'),
 ('YB2HCLb_NEi4xGNjTrDf-Q', 'Inginerie Software', 'tcKjwZ5sc0W38PNcZVFf9Q', 'MASTER'),
 ('1D8lg4-opkWoZBd1JmH58A', 'Baze de Date', 'tcKjwZ5sc0W38PNcZVFf9Q', 'MASTER'),
 ('-bFu2-I2FEmGydJJgOqsgA', 'Inginerie Software', 'eKCAcyihP0qCgiRlGnPMzw', 'PhD'),
 ('uUYdxLom40etamiScr6j1w', 'Matematica si Informatica', 'tcKjwZ5sc0W38PNcZVFf9Q', 'PhD');

 /*Populate sessions*/
 insert into evl_evlses (id, n, regst, regend, delst, delend, pbid)
 values
 ('P9fgTR_qpEG3UwhlzJ9GFQ', 'Internship Martie', '2021-03-15 00:00:00.000000', '2021-03-28 00:00:00.000000', '2021-03-28 00:00:00.000000', '2021-03-31 00:00:00.000000', '2HZobZTU0kO80jUGt_k4lA'),
 ('NyjHQfF200m4fkbQW2GsXQ', 'Internship August', '2021-08-25 00:00:00.000000', '2021-08-30 00:00:00.000000', '2021-08-31 00:00:00.000000', '2021-09-01 00:00:00.000000', 'W02sEusXv0ejYl5aZPDnuQ'),
 ('UeIYt5Yctkm6Qhhckj9bCQ', 'Internship Septembrie', '2021-08-15 00:00:00.000000', '2021-08-28 00:00:00.000000', '2021-08-25 00:00:00.000000', '2021-08-31 00:00:00.000000', 'W02sEusXv0ejYl5aZPDnuQ');
