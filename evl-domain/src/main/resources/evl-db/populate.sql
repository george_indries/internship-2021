 /*Populate universities*/
 insert into evl_unv (id, n)
 values
 ('tcKjwZ5sc0W38PNcZVFf9Q', 'UBB'),
 ('eKCAcyihP0qCgiRlGnPMzw', 'UTCN');
 
 /*Populate majors*/
 insert into evl_mj (id, n, unvid, ql)
 values
 ('sXm6Tdh2K0CV12gNz0nv2g', 'Automatica', 'eKCAcyihP0qCgiRlGnPMzw', 'BACHELOR'),
 ('1NrN_dtKD0SKHW27rS81cQ', 'Calculatoare', 'eKCAcyihP0qCgiRlGnPMzw', 'BACHELOR'),
 ('Lh9qKtBWBkSx-BY2pgRIEw', 'Informatica', 'tcKjwZ5sc0W38PNcZVFf9Q', 'BACHELOR'),
 ('U4NYm7nowkeuE0SeO66eLA', 'Matematica', 'tcKjwZ5sc0W38PNcZVFf9Q', 'BACHELOR');
 
 /*Populate problems*/
 insert into evl_pb (id, txt, n, descr)
 values
 ('2HZobZTU0kO80jUGt_k4lA', 'Se dau doua siruri a si b, cu n, respectiv m elemente, numere naturale, ordonate crescator. Sa se construiasca un al treilea sir, c, care sa contina, in ordine crescatoare, elementele din sirurile a si b.', 'Interclasare', 'Consideram doua tablouri unidimensionale cu elemente numere intregi ordonate crescator. Se doreste construirea unui alt tablou, care sa contina valorile din cele doua tablouri, in ordine.'),
 ('W02sEusXv0ejYl5aZPDnuQ', 'Se da un sir cu n elemente, numere intregi. Folosind metoda QuickSort (Sortare Rapida), ordonati crescator elementele acestui sir.', 'QuickSort', 'Programul citeste de la tastatura numarul n, iar apoi cele n elemente ale sirului.');
 
 /*Populate sessions*/
 insert into evl_evlses (id, n, regst, regend, delst, delend, pbid)
 values
 ('P9fgTR_qpEG3UwhlzJ9GFQ', 'Internship 2020', '2020-03-15 00:00:00.000000', '2020-03-31 00:00:00.000000', '2020-04-01 00:00:00.000000', '2020-04-04 00:00:00.000000', '2HZobZTU0kO80jUGt_k4lA'),
 ('UeIYt5Yctkm6Qhhckj9bCQ', 'Internship 2021', '2021-03-15 00:00:00.000000', '2021-03-31 00:00:00.000000', '2021-04-01 00:00:00.000000', '2021-04-04 00:00:00.000000', 'W02sEusXv0ejYl5aZPDnuQ');
 
 /*Populate candidates*/
 insert into evl_cnd (id, fnm, lnm, eml, pswd, unvn, mjn, ql, ay, regdt, deldt, unvid, mjid, evlsesid)
 values
 ('vZz3NhGqcEOJ9DJdIE1hxg', 'Cosmin', 'Handaric', 'cosmin.handaric22@gmail.com', 'abcdef', 'UTCN', 'Calculatoare', 'BACHELOR', 'FOURTH', '2021-03-16 17:30:00.000000', null, 'eKCAcyihP0qCgiRlGnPMzw', '1NrN_dtKD0SKHW27rS81cQ', 'UeIYt5Yctkm6Qhhckj9bCQ');