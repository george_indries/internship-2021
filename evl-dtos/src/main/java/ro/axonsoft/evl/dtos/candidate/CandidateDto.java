package ro.axonsoft.evl.dtos.candidate;

import lombok.Data;
import ro.axonsoft.evl.enums.AttendanceYear;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class CandidateDto {

    private String id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @Email
    private String email;

    @NotNull
    private String password;

    @NotNull
    private String universityName;

    private String universityId;

    @NotNull
    private String majorName;

    private String majorId;

    @NotNull
    private String evlSessionId;

    @NotNull
    private String qualificationLevel;

    @NotNull
    private AttendanceYear attendanceYear;
}
