package ro.axonsoft.evl.dtos.evlsession;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EvlSessionDto {

    private String id;

    @NotNull
    private String name;

    private String problemId;
}
