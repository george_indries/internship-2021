package ro.axonsoft.evl.dtos.major;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MajorDto {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String qualificationLevel;

    @NotNull
    private String universityId;
}
