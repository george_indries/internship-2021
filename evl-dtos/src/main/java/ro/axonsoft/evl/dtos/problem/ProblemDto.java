package ro.axonsoft.evl.dtos.problem;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProblemDto {

    private String id;

    @NotNull
    private String text;

    private String name;

    private String description;
}
