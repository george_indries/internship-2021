package ro.axonsoft.evl.dtos.university;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UniversityDto {

    private String id;

    @NotNull
    private String name;
}
