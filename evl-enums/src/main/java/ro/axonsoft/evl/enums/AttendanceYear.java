package ro.axonsoft.evl.enums;

public enum AttendanceYear {
    GRADUATE,
    FIRST,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH,
    SIXTH
}
