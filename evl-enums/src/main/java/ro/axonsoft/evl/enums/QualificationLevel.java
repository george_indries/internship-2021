package ro.axonsoft.evl.enums;

public enum QualificationLevel {
    BACHELOR,
    MASTER,
    PhD
}
