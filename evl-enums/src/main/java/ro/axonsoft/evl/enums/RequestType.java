package ro.axonsoft.evl.enums;

public enum RequestType {
    AUTHENTICATION,
    AUTHORIZATION
}
