package ro.axonsoft.evl.exceptions;

public class CandidateAlreadyRegisteredException extends RuntimeException {

    public CandidateAlreadyRegisteredException() {
    }

    public CandidateAlreadyRegisteredException(String message) {
        super(message);
    }

    public CandidateAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }
}
