package ro.axonsoft.evl.exceptions;

public class InvalidFileNameException extends RuntimeException {

    public InvalidFileNameException() {
    }

    public InvalidFileNameException(String message) {
        super(message);
    }

    public InvalidFileNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
