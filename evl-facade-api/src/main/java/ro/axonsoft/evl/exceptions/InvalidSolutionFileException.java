package ro.axonsoft.evl.exceptions;

public class InvalidSolutionFileException extends RuntimeException {

    public InvalidSolutionFileException() {
    }

    public InvalidSolutionFileException(String message) {
        super(message);
    }

    public InvalidSolutionFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
