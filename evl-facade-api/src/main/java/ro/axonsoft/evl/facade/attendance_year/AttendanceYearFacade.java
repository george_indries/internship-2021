package ro.axonsoft.evl.facade.attendance_year;

import java.util.List;

public interface AttendanceYearFacade {

    List<String> findAllAttendanceYears();
}
