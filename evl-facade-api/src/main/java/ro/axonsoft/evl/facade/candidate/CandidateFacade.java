package ro.axonsoft.evl.facade.candidate;

import ro.axonsoft.evl.dtos.candidate.CandidateDto;

public interface CandidateFacade {

    CandidateDto save(CandidateDto candidateDto);
}
