package ro.axonsoft.evl.facade.evlsession;

import ro.axonsoft.evl.dtos.evlsession.EvlSessionDto;
import ro.axonsoft.evl.enums.PeriodType;

import java.util.List;

public interface EvlSessionFacade {

    List<EvlSessionDto> filterByPeriodType(PeriodType periodType);
}
