package ro.axonsoft.evl.facade.major;

import ro.axonsoft.evl.dtos.major.MajorDto;

import java.util.List;

public interface MajorFacade {

    List<MajorDto> findByUniversityIdAndQualificationLevel(String universityId, String qualificationLevel);
}
