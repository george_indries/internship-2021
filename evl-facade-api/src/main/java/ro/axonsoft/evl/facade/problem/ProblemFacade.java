package ro.axonsoft.evl.facade.problem;

import ro.axonsoft.evl.dtos.problem.ProblemDto;

import java.io.IOException;

public interface ProblemFacade {

    ProblemDto findByEvaluationSessionId(String evlSessionId);

    String problemMdToHTMLConversion(ProblemDto problemDto);

    void saveSolution(String solutionName, byte[] fileContent, String email, String evlSessionId) throws IOException;
}
