package ro.axonsoft.evl.facade.qualification_level;

import java.util.List;

public interface QualificationLevelFacade {

    List<String> findAllQualificationLevels();
}
