package ro.axonsoft.evl.facade.university;

import ro.axonsoft.evl.dtos.university.UniversityDto;

import java.util.List;

public interface UniversityFacade {

    List<UniversityDto> findAll();
}
