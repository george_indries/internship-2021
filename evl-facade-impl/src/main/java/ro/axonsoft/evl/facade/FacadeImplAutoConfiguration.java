package ro.axonsoft.evl.facade;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ro.axonsoft.evl.facade.candidate.CandidateFactory;
import ro.axonsoft.evl.facade.evlsession.EvlSessionFactory;
import ro.axonsoft.evl.facade.major.MajorFactory;
import ro.axonsoft.evl.facade.problem.ProblemFactory;
import ro.axonsoft.evl.facade.university.UniversityFactory;

@Configuration
@Import({CandidateFactory.class, EvlSessionFactory.class, MajorFactory.class, ProblemFactory.class,
        UniversityFactory.class})
@ComponentScan(basePackages = {"ro.axonsoft.evl.facade.*"})
public class FacadeImplAutoConfiguration {
}
