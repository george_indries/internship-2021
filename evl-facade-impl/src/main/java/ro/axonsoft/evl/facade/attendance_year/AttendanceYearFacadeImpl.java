package ro.axonsoft.evl.facade.attendance_year;

import ro.axonsoft.evl.enums.AttendanceYear;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AttendanceYearFacadeImpl implements AttendanceYearFacade {
    @Override
    public List<String> findAllAttendanceYears() {
        return Arrays.stream(AttendanceYear.values()).map(Enum::name).collect(Collectors.toList());
    }
}
