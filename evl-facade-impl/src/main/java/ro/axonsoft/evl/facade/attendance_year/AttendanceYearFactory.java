package ro.axonsoft.evl.facade.attendance_year;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AttendanceYearFactory {

    @Bean
    AttendanceYearFacade attendanceYearFacade() {
        return new AttendanceYearFacadeImpl();
    }
}
