package ro.axonsoft.evl.facade.candidate;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.candidate.CandidateRepository;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.evlsession.EvlSessionRepository;
import ro.axonsoft.evl.dtos.candidate.CandidateDto;
import ro.axonsoft.evl.enums.ExceptionType;
import ro.axonsoft.evl.exceptions.CandidateAlreadyRegisteredException;
import ro.axonsoft.evl.exceptions.InternalServerException;
import ro.axonsoft.evl.exceptions.InvalidSessionException;

import java.time.LocalDateTime;

@Component
public class CandidateFacadeImpl implements CandidateFacade {

    private CandidateRepository candidateRepository;
    private CandidateMapper candidateMapper;
    private EvlSessionRepository evlSessionRepository;

    @Autowired
    public void setEvlSessionRepository(EvlSessionRepository evlSessionRepository) {
        this.evlSessionRepository = evlSessionRepository;
    }

    @Autowired
    public void setCandidateRepository(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Autowired
    public void setCandidateMapper(CandidateMapper candidateMapper) {
        this.candidateMapper = candidateMapper;
    }

    @Override
    @Transactional
    public CandidateDto save(CandidateDto candidateDto) {
        Candidate candidate = candidateMapper.candidateDtoToCandidate(candidateDto);
        candidate.setPassword(BCrypt.hashpw(candidate.getPassword(), BCrypt.gensalt()));
        LocalDateTime current = LocalDateTime.now();
        candidate.setRegistrationDate(current);
        EvlSession session = evlSessionRepository.findById(candidateDto.getEvlSessionId()).orElse(null);
        if(session == null)
            throw new InternalServerException(ExceptionType.INTERNAL_SERVER_ERROR.toString());
        if(current.isBefore(session.getRegistrationStart()) || current.isAfter(session.getRegistrationEnd()))
            throw new InvalidSessionException(ExceptionType.INVALID_SESSION.toString());
        if(candidateRepository.existsByEmailAndSessionId(candidate.getEmail(), candidate.getEvlSession().getId()))
            throw new CandidateAlreadyRegisteredException(ExceptionType.CANDIDATE_ALREADY_REGISTERED.toString());
        Candidate savedCandidate = candidateRepository.save(candidate);
        return candidateMapper.candidateToCandidateDto(savedCandidate);
    }
}
