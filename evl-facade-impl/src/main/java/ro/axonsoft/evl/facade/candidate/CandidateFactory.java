package ro.axonsoft.evl.facade.candidate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CandidateFactory {

    @Bean
    public CandidateFacade candidateFacade() {
        return new CandidateFacadeImpl();
    }

    @Bean
    public CandidateMapper candidateMapper() {
        return new CandidateMapperImpl();
    }
}
