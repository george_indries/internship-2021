package ro.axonsoft.evl.facade.candidate;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.dtos.candidate.CandidateDto;

@Mapper
@DecoratedWith(CandidateMapperDecorator.class)
public interface CandidateMapper {

    CandidateMapper INSTANCE = Mappers.getMapper(CandidateMapper.class);

    @Mapping(source = "evlSession.id", target = "evlSessionId")
    @Mapping(source = "major.id", target = "majorId")
    @Mapping(source = "university.id", target = "universityId")
    CandidateDto candidateToCandidateDto(Candidate candidate);

    @Mapping(target = "university", ignore = true)
    @Mapping(target = "major", ignore = true)
    @Mapping(target = "evlSession", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "deliveryDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    Candidate candidateDtoToCandidate(CandidateDto candidateDto);
}
