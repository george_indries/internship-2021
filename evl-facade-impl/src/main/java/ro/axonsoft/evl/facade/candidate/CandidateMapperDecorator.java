package ro.axonsoft.evl.facade.candidate;

import org.springframework.beans.factory.annotation.Autowired;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.evlsession.EvlSessionRepository;
import ro.axonsoft.evl.domain.major.Major;
import ro.axonsoft.evl.domain.major.MajorRepository;
import ro.axonsoft.evl.domain.university.University;
import ro.axonsoft.evl.domain.university.UniversityRepository;
import ro.axonsoft.evl.dtos.candidate.CandidateDto;

public abstract class CandidateMapperDecorator implements CandidateMapper {

    private final CandidateMapper candidateMapper;
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private MajorRepository majorRepository;
    @Autowired
    private EvlSessionRepository evlSessionRepository;

    public CandidateMapperDecorator(CandidateMapper candidateMapper) {
        this.candidateMapper = candidateMapper;
    }

    @Override
    public Candidate candidateDtoToCandidate(CandidateDto candidateDto) {

        Candidate candidate = candidateMapper.candidateDtoToCandidate(candidateDto);

        EvlSession evlSession = evlSessionRepository.findById(candidateDto.getEvlSessionId()).orElse(null);
        candidate.setEvlSession(evlSession);

        if(candidateDto.getUniversityId() == null) {
            candidate.setUniversityName(candidateDto.getUniversityName());
        } else {
            University university = universityRepository.findById(candidateDto.getUniversityId()).orElse(null);
            candidate.setUniversity(university);
        }

        if(candidateDto.getMajorId() == null) {
            candidate.setMajorName(candidateDto.getMajorName());
        } else {
            Major major = majorRepository.findById(candidateDto.getMajorId()).orElse(null);
            candidate.setMajor(major);
        }

        return candidate;
    }
}
