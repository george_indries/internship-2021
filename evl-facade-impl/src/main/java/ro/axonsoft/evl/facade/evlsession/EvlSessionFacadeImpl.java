package ro.axonsoft.evl.facade.evlsession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.evlsession.EvlSessionRepository;
import ro.axonsoft.evl.dtos.evlsession.EvlSessionDto;
import ro.axonsoft.evl.enums.PeriodType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class EvlSessionFacadeImpl implements EvlSessionFacade {

    private EvlSessionRepository evlSessionRepository;
    private EvlSessionMapper evlSessionMapper;

    @Autowired
    public void setEvlSessionRepository(EvlSessionRepository evlSessionRepository) {
        this.evlSessionRepository = evlSessionRepository;
    }

    @Autowired
    public void setEvlSessionMapper(EvlSessionMapper evlSessionMapper) {
        this.evlSessionMapper = evlSessionMapper;
    }

    @Override
    public List<EvlSessionDto> filterByPeriodType(PeriodType periodType) {
        LocalDateTime current = LocalDateTime.now();
        List<EvlSession> sessions;
        if(periodType.equals(PeriodType.REGISTER)) {
            sessions = evlSessionRepository.findAllActiveSessionsForRegistration(current);
        } else {
            sessions = evlSessionRepository.findAllActiveSessionsForDelivery(current);
        }
        return sessions.stream()
                .map(evlSessionMapper::evlSessionToEvlSessionDto)
                .collect(Collectors.toList());
    }
}
