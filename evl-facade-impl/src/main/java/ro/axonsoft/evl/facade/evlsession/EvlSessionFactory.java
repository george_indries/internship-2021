package ro.axonsoft.evl.facade.evlsession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EvlSessionFactory {

    @Bean
    public EvlSessionFacade evlSessionFacade() {
        return new EvlSessionFacadeImpl();
    }

    @Bean
    public EvlSessionMapper evlSessionMapper() {
        return new EvlSessionMapperImpl();
    }
}
