package ro.axonsoft.evl.facade.evlsession;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.dtos.evlsession.EvlSessionDto;

@Mapper
@DecoratedWith(EvlSessionMapperDecorator.class)
public interface EvlSessionMapper {

    EvlSessionMapper INSTANCE = Mappers.getMapper(EvlSessionMapper.class);

    @Mapping(source = "problem.id", target = "problemId")
    EvlSessionDto evlSessionToEvlSessionDto(EvlSession evlSession);

    @Mapping(target = "problem", ignore = true)
    @Mapping(target = "candidates", ignore = true)
    @Mapping(target = "registrationStart", ignore = true)
    @Mapping(target = "registrationEnd", ignore = true)
    @Mapping(target = "delStart", ignore = true)
    @Mapping(target = "delEnd", ignore = true)
    @Mapping(target = "id", ignore = true)
    EvlSession evlSessionDtoToEvlSession(EvlSessionDto evlSessionDto);
}
