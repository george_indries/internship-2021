package ro.axonsoft.evl.facade.evlsession;

import org.springframework.beans.factory.annotation.Autowired;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.evlsession.EvlSessionRepository;
import ro.axonsoft.evl.dtos.evlsession.EvlSessionDto;

public abstract class EvlSessionMapperDecorator implements EvlSessionMapper {

    private final EvlSessionMapper evlSessionMapper;

    @Autowired
    private EvlSessionRepository evlSessionRepository;

    protected EvlSessionMapperDecorator(EvlSessionMapper evlSessionMapper) {
        this.evlSessionMapper = evlSessionMapper;
    }

    @Override
    public EvlSession evlSessionDtoToEvlSession(EvlSessionDto evlSessionDto) {

        return evlSessionRepository.findById(evlSessionDto.getId()).orElse(null);
    }
}
