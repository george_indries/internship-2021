package ro.axonsoft.evl.facade.major;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.axonsoft.evl.domain.major.Major;
import ro.axonsoft.evl.domain.major.MajorRepository;
import ro.axonsoft.evl.dtos.major.MajorDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MajorFacadeImpl implements MajorFacade {

    private MajorRepository majorRepository;
    private MajorMapper majorMapper;

    @Autowired
    public void setMajorRepository(MajorRepository majorRepository) {
        this.majorRepository = majorRepository;
    }

    @Autowired
    public void setMajorMapper(MajorMapper majorMapper) {
        this.majorMapper = majorMapper;
    }

    private List<MajorDto> mapMajorsToMajorsDTO(List<Major> majors) {
        return majors.stream()
                .map(majorMapper::majorToMajorDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<MajorDto> findByUniversityIdAndQualificationLevel(String universityId, String qualificationLevel) {
        if (qualificationLevel == null) {
            return mapMajorsToMajorsDTO(majorRepository.findByUniversityId(universityId));
        }
        return mapMajorsToMajorsDTO(majorRepository.findByUniversityIdAndQualificationLevel(universityId, qualificationLevel));
    }
}
