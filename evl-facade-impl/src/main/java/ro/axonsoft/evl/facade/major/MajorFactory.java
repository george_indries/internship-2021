package ro.axonsoft.evl.facade.major;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MajorFactory {

    @Bean
    public MajorFacade majorFacade() {
        return new MajorFacadeImpl();
    }

    @Bean
    public MajorMapper majorMapper() {
        return new MajorMapperImpl();
    }
}
