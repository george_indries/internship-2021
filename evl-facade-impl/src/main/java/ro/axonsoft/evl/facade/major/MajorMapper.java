package ro.axonsoft.evl.facade.major;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ro.axonsoft.evl.domain.major.Major;
import ro.axonsoft.evl.dtos.major.MajorDto;

@Mapper
@DecoratedWith(MajorMapperDecorator.class)
public interface MajorMapper {

    MajorMapper INSTANCE = Mappers.getMapper(MajorMapper.class);

    @Mapping(source = "university.id", target = "universityId")
    MajorDto majorToMajorDto(Major major);

    @Mapping(target = "university", ignore = true)
    @Mapping(target = "candidates", ignore = true)
    @Mapping(target = "id", ignore = true)
    Major majorDtoToMajor(MajorDto majorDto);
}
