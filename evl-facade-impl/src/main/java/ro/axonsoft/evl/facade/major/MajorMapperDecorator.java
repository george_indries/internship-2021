package ro.axonsoft.evl.facade.major;

import org.springframework.beans.factory.annotation.Autowired;
import ro.axonsoft.evl.domain.major.Major;
import ro.axonsoft.evl.domain.major.MajorRepository;
import ro.axonsoft.evl.dtos.major.MajorDto;

public abstract class MajorMapperDecorator implements MajorMapper {

    private final MajorMapper majorMapper;

    @Autowired
    private MajorRepository majorRepository;

    protected MajorMapperDecorator(MajorMapper majorMapper) {
        this.majorMapper = majorMapper;
    }

    @Override
    public Major majorDtoToMajor(MajorDto majorDto) {

       return majorRepository.findById(majorDto.getId()).orElse(null);
    }
}
