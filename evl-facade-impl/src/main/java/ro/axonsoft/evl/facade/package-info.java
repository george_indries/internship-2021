
@org.immutables.value.Value.Style(//
        typeImmutable = "Imt*", //
        deepImmutablesDetection = true, //
		get = {"is*", "get*"}
)
package ro.axonsoft.evl.facade;