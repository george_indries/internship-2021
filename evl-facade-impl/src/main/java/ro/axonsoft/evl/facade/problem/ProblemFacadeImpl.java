package ro.axonsoft.evl.facade.problem;

import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.vladsch.flexmark.util.misc.ImageUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ro.axonsoft.evl.domain.candidate.Candidate;
import ro.axonsoft.evl.domain.candidate.CandidateRepository;
import ro.axonsoft.evl.domain.evlsession.EvlSession;
import ro.axonsoft.evl.domain.evlsession.EvlSessionRepository;
import ro.axonsoft.evl.domain.problem.Problem;
import ro.axonsoft.evl.domain.problem.ProblemRepository;
import ro.axonsoft.evl.dtos.problem.ProblemDto;
import net.sourceforge.plantuml.SourceStringReader;
import ro.axonsoft.evl.enums.ExceptionType;
import ro.axonsoft.evl.exceptions.InvalidSessionException;
import ro.axonsoft.evl.exceptions.InternalServerException;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class ProblemFacadeImpl implements ProblemFacade {

    @Value("${file.upload-dir}")
    String FILE_DIRECTORY;

    private ProblemRepository problemRepository;
    private ProblemMapper problemMapper;
    private EvlSessionRepository evlSessionRepository;
    private CandidateRepository candidateRepository;

    @Autowired
    public void setEvlSessionRepository(EvlSessionRepository evlSessionRepository) {
        this.evlSessionRepository = evlSessionRepository;
    }

    @Autowired
    public void setProblemRepository(ProblemRepository problemRepository) {
        this.problemRepository = problemRepository;
    }

    @Autowired
    public void setProblemMapper(ProblemMapper problemMapper) {
        this.problemMapper = problemMapper;
    }

    @Autowired
    public void setCandidateRepository(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public ProblemDto findByEvaluationSessionId(String evlSessionId) {
        EvlSession evlSession = evlSessionRepository.findById(evlSessionId).orElse(null);
        validateEvlSession(evlSession, LocalDateTime.now());
        return problemMapper.problemToProblemDto(problemRepository.findById(evlSession.getProblem().getId()).orElse(null));
    }

    private void validateEvlSession(EvlSession evlSession, LocalDateTime currentDate) {
        if (evlSession == null) {
            throw new InternalServerException(ExceptionType.INTERNAL_SERVER_ERROR.toString());
        }
        if(evlSession.getDelStart().isAfter(currentDate) || evlSession.getDelEnd().isBefore(currentDate))
            throw new InvalidSessionException(ExceptionType.INVALID_SESSION.toString());
    }

    @Override
    public String problemMdToHTMLConversion(ProblemDto problemDto) {
        MutableDataSet options = new MutableDataSet();
        options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create()));
        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();

        String text = problemDto.getText();
        String res = convertUMLsToImages(text, problemDto.getId());
        Node document = parser.parse(res);
        String result = setIdsForContent(renderer.render(document));

        return "<h1>" + problemDto.getName() + "</h1>\n" + problemDto.getDescription() + "\n" + result;
    }

    @Transactional
    @Override
    public void saveSolution(String solutionName, byte[] fileContent, String email, String evlSessionId) throws IOException {
        Candidate candidate = candidateRepository.findByEmail(email);
        EvlSession session = evlSessionRepository.findById(evlSessionId).orElse(null);
        LocalDateTime currentDate = LocalDateTime.now();
        validateEvlSession(session, currentDate);
        candidateRepository.update(candidate.getId(), currentDate);
        String extension = FilenameUtils.getExtension(solutionName);
        String solution = candidate.getId() + "_" + candidate.getLastName() + "_" + candidate.getFirstName() + "." + extension;
        File newFile = new File(FILE_DIRECTORY + evlSessionId + "\\" + solution);

        FileOutputStream outputStream = new FileOutputStream(newFile);
        outputStream.write(fileContent);
        outputStream.close();
    }


    private String convertUMLsToImages(String text, String problemId) {
        int diagramCount = 1;
        while (text.contains("@startuml")) {
            String diagram = text.substring(text.indexOf("@startuml"), text.indexOf("@enduml") + 7);
            String imageTitle = null;
            if (diagram.contains("title")) {
                imageTitle = diagram.lines()
                        .filter(line -> line.contains("title"))
                        .findFirst()
                        .map(line -> line.substring(line.indexOf("title") + 6)).orElse(null);
            }

            try {
                Path path1 = Paths.get("static");
                String diagramName = "diagram" + diagramCount + ".png";
                String path = path1.toAbsolutePath().toString() + "\\problems\\" + problemId + "\\images\\" + diagramName;

                File png = new File(path);

                SourceStringReader reader = new SourceStringReader(diagram);
                reader.generateImage(png);
                String imageUri = ImageUtils.base64Encode(png);

                String newValue = "\n<img src=\"" + imageUri + "\" alt=\"Image not found\">\n";
                if (imageTitle != null) {
                    newValue = "\n<img src=\"" + imageUri + "\" alt=\"Image not found\" title=\"" + imageTitle + "\">\n";
                }

                text = text.replace(diagram, newValue);
            } catch (IOException e) {
                e.printStackTrace();
            }
            diagramCount++;
        }
        return text;
    }


    private String setIdsForContent(String text) {
        return text.lines()
                .map(line -> {
                    if (line.contains("<!--id=")) {
                        String id = line.substring(line.indexOf("=\"") + 1, line.indexOf("-->"));
                        String name = line.substring(line.indexOf("<h") + 4, line.indexOf("<!--id"));
                        String hType = line.substring(line.indexOf("<h") + 2, line.indexOf('>'));
                        return "<h" + hType + " id=" + id + ">" + name + "</h" + hType + ">";
                    }
                    return line;
                }).collect(Collectors.joining("\n"));
    }
}
