package ro.axonsoft.evl.facade.problem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProblemFactory {

    @Bean
    public ProblemFacade problemFacade() {
        return new ProblemFacadeImpl();
    }

    @Bean
    public ProblemMapper problemMapper() {
        return new ProblemMapperImpl();
    }

}
