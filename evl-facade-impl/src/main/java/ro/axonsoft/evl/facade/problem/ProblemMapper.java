package ro.axonsoft.evl.facade.problem;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ro.axonsoft.evl.domain.problem.Problem;
import ro.axonsoft.evl.dtos.problem.ProblemDto;

@Mapper
@DecoratedWith(ProblemMapperDecorator.class)
public interface ProblemMapper {

    ProblemMapper INSTANCE = Mappers.getMapper(ProblemMapper.class);

    ProblemDto problemToProblemDto(Problem problem);

    @Mapping(target = "evlSessions", ignore = true)
    @Mapping(target = "id", ignore = true)
    Problem problemDtoToProblem(ProblemDto problemDto);
}
