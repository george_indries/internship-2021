package ro.axonsoft.evl.facade.problem;

import ro.axonsoft.evl.domain.problem.Problem;
import ro.axonsoft.evl.dtos.problem.ProblemDto;

public abstract class ProblemMapperDecorator implements ProblemMapper {

    private final ProblemMapper problemMapper;

    protected ProblemMapperDecorator(ProblemMapper problemMapper) {
        this.problemMapper = problemMapper;
    }

    @Override
    public Problem problemDtoToProblem(ProblemDto problemDto) {

        return problemMapper.problemDtoToProblem(problemDto);
    }
}
