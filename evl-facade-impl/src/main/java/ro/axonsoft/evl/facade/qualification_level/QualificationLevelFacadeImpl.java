package ro.axonsoft.evl.facade.qualification_level;

import ro.axonsoft.evl.enums.QualificationLevel;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class QualificationLevelFacadeImpl implements QualificationLevelFacade {
    @Override
    public List<String> findAllQualificationLevels() {
        return Arrays.stream(QualificationLevel.values()).map(Enum::name).collect(Collectors.toList());
    }
}
