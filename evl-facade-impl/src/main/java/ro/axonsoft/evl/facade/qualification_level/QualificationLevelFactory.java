package ro.axonsoft.evl.facade.qualification_level;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QualificationLevelFactory {

    @Bean
    QualificationLevelFacade qualificationLevelFacade() {
        return new QualificationLevelFacadeImpl();
    }
}
