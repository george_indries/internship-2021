package ro.axonsoft.evl.facade.university;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.axonsoft.evl.domain.university.UniversityRepository;
import ro.axonsoft.evl.dtos.university.UniversityDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UniversityFacadeImpl implements UniversityFacade {

    private UniversityRepository universityRepository;
    private UniversityMapper universityMapper;

    @Autowired
    public void setUniversityRepository(UniversityRepository universityRepository) {
        this.universityRepository = universityRepository;
    }

    @Autowired
    public void setUniversityMapper(UniversityMapper universityMapper) {
        this.universityMapper = universityMapper;
    }

    @Override
    public List<UniversityDto> findAll() {
        return universityRepository.findAll()
                .stream()
                .map(universityMapper::universityToUniversityDto)
                .collect(Collectors.toList());
    }
}
