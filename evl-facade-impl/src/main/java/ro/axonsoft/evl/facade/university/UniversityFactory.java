package ro.axonsoft.evl.facade.university;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UniversityFactory {

    @Bean
    public UniversityFacade universityFacade() {
        return new UniversityFacadeImpl();
    }

    @Bean
    public UniversityMapper universityMapper() {
        return new UniversityMapperImpl();
    }
}
