package ro.axonsoft.evl.facade.university;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ro.axonsoft.evl.domain.university.University;
import ro.axonsoft.evl.dtos.university.UniversityDto;

@Mapper
@DecoratedWith(UniversityMapperDecorator.class)
public interface UniversityMapper {

    UniversityMapper INSTANCE = Mappers.getMapper(UniversityMapper.class);

    UniversityDto universityToUniversityDto(University university);

    @Mapping(target = "majors", ignore = true)
    @Mapping(target = "candidates", ignore = true)
    @Mapping(target = "id", ignore = true)
    University universityDtoToUniversity(UniversityDto universityDto);
}
