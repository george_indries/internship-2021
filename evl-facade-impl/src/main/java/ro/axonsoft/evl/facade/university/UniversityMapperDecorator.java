package ro.axonsoft.evl.facade.university;

import org.springframework.beans.factory.annotation.Autowired;
import ro.axonsoft.evl.domain.university.University;
import ro.axonsoft.evl.domain.university.UniversityRepository;
import ro.axonsoft.evl.dtos.university.UniversityDto;

public abstract class UniversityMapperDecorator implements UniversityMapper {

    private final UniversityMapper universityMapper;
    @Autowired
    private UniversityRepository universityRepository;

    protected UniversityMapperDecorator(UniversityMapper universityMapper) {
        this.universityMapper = universityMapper;
    }

    @Override
    public University universityDtoToUniversity(UniversityDto universityDto) {

        return universityRepository.findById(universityDto.getId()).orElse(null);
    }
}
